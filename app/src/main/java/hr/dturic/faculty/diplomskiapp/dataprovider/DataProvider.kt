package hr.dturic.faculty.diplomskiapp.dataprovider

import hr.dturic.faculty.diplomskiapp.models.Pokemon
import hr.dturic.faculty.diplomskiapp.models.PokemonListResponse
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object DataProvider {
    private val service: HttpService

    init {
        val retrofit =
            Retrofit.Builder()
                .baseUrl("https://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(OkHttpClient())
                .build()

        service = retrofit.create(HttpService::class.java)
    }

    fun fetchPokemons(): Call<PokemonListResponse> {
        return service.fetchPokemons()
    }

    fun fetchPokemon(name: String): Call<Pokemon> {
        return service.fetchPokemon(name)
    }
}