package hr.dturic.faculty.diplomskiapp.list

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import hr.dturic.faculty.diplomskiapp.R
import hr.dturic.faculty.diplomskiapp.base.ActivityContract
import hr.dturic.faculty.diplomskiapp.base.LoadingFragment
import hr.dturic.faculty.diplomskiapp.base.ModelObserver
import hr.dturic.faculty.diplomskiapp.models.PokemonList
import kotlinx.android.synthetic.main.fragment_list.*

class ListFragment : Fragment(), ModelObserver, PokemonListAdapter.OnPokemonSelectedListener {

    companion object {

        fun newInstance(model: ListModel) = ListFragment().also {
            it.model = model
        }
    }

    private var model: ListModel? = null
    private var contract: ActivityContract? = null
    private var adapter = PokemonListAdapter(ArrayList(), this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        this.model?.subscribe(this)

        if (context is ActivityContract) {
            this.contract = context
        }
    }

    override fun onDetach() {
        model?.unsubscribe(this)
        contract = null

        super.onDetach()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        pokemonList.layoutManager = LinearLayoutManager(requireContext())
        pokemonList.adapter = this.adapter

        refresh.setOnRefreshListener {
            contract?.handleEvent(ListActivity.REFRESH, Any())
            refresh.isRefreshing = false
        }

        search.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                searchBar.visibility = View.VISIBLE
                toolbarTitle.visibility = View.GONE
            } else {
                searchBar.visibility = View.GONE
                toolbarTitle.visibility = View.VISIBLE
            }
        }

        searchBar.addTextChangedListener(getTextWatcher())
    }

    override fun showLoading(load: Boolean) {
        if (load) {
            childFragmentManager.beginTransaction()
                .add(content.id, LoadingFragment(), "loading")
                .commit()
        } else {
            val loader = childFragmentManager.findFragmentByTag("loading")
            if (loader != null) childFragmentManager.beginTransaction().remove(loader).commit()
        }
    }

    override fun update(flag: Int) {
        if (flag == ListModel.POKEMONS) {
            val pokemons = model?.pokemons
            adapter.changeData(pokemons)
        }
    }

    override fun onPokemonSelected(pokemon: PokemonList) {
        contract?.handleEvent(ListActivity.POKEMON_SELECTED, pokemon.codeName)
    }

    private fun getTextWatcher() = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            if (s != null) {
                adapter.filter(s.toString())
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }
    }
}
