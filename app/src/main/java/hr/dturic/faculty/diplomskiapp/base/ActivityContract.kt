package hr.dturic.faculty.diplomskiapp.base

interface ActivityContract {
    fun handleEvent(event: Int, value: Any)
    fun finishActivity()
}