package hr.dturic.faculty.diplomskiapp.models

import com.google.gson.annotations.SerializedName

data class PokemonListResponse(
    @SerializedName("count") var name: Int,
    @SerializedName("next") var next: String,
    @SerializedName("previous") var previous: String,
    @SerializedName("results") var results: ArrayList<PokemonListResult>
)