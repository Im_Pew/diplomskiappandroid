package hr.dturic.faculty.diplomskiapp.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hr.dturic.faculty.diplomskiapp.R
import hr.dturic.faculty.diplomskiapp.models.PokemonList

class PokemonListAdapter(
    private var pokemons: ArrayList<PokemonList>,
    private val listener: OnPokemonSelectedListener
) :
    RecyclerView.Adapter<PokemonListViewHolder>() {

    private var filteredData = ArrayList<PokemonList>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonListViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.pokemon_list_item, parent, false)
        return PokemonListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return filteredData.size
    }

    override fun onBindViewHolder(holder: PokemonListViewHolder, position: Int) {
        holder.bind(filteredData[position], listener)
    }

    fun changeData(pokemons: java.util.ArrayList<PokemonList>?) {
        this.filteredData = pokemons ?: ArrayList()
        this.pokemons = pokemons ?: ArrayList()
        notifyDataSetChanged()
    }

    fun filter(filter: String) {

        filteredData = if (filter == "") pokemons
        else ArrayList(pokemons.filter { pokemonList ->
            pokemonList.name.contains(filter, true)
        })

        notifyDataSetChanged()
    }

    interface OnPokemonSelectedListener {
        fun onPokemonSelected(pokemon: PokemonList)
    }
}