package hr.dturic.faculty.diplomskiapp.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Images(
    @SerializedName("photo") private var photo: String,
    @SerializedName("typeIcon") private var typeIcon: String,
    @SerializedName("weaknessIcon") private var weaknessIcon: String
): Serializable {
    companion object {
        const val BASE_URL = "https://courses.cs.washington.edu/courses/cse154/webservices/pokedex/"
    }

    fun getPhoto() = BASE_URL + photo
    fun getTypeIcon() = BASE_URL + typeIcon
    fun getWeaknessIcon() = BASE_URL + weaknessIcon
}
