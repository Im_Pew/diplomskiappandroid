package hr.dturic.faculty.diplomskiapp.list

import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import hr.dturic.faculty.diplomskiapp.R
import hr.dturic.faculty.diplomskiapp.models.PokemonList
import kotlinx.android.extensions.LayoutContainer

class PokemonListViewHolder(view: View) : RecyclerView.ViewHolder(view), LayoutContainer {
    override val containerView: View = itemView

    private val pokemonName: TextView = view.findViewById(R.id.pokemonName)
    private val pokemonIcon: AppCompatImageView = view.findViewById(R.id.pokemonIcon)

    private val context = view.context

    fun bind(pokemon: PokemonList, listener: PokemonListAdapter.OnPokemonSelectedListener) {
        pokemonName.text = pokemon.name

        Glide.with(context)
            .load(pokemon.url)
            .into(pokemonIcon)

        itemView.setOnClickListener {
            listener.onPokemonSelected(pokemon)
        }
    }
}