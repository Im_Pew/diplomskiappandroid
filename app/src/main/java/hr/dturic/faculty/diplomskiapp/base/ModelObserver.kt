package hr.dturic.faculty.diplomskiapp.base

interface ModelObserver {
    fun update(flag: Int)
    fun showLoading(load: Boolean)
}