package hr.dturic.faculty.diplomskiapp.list

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import hr.dturic.faculty.diplomskiapp.R
import hr.dturic.faculty.diplomskiapp.base.ActivityContract
import hr.dturic.faculty.diplomskiapp.base.ModelObserver
import hr.dturic.faculty.diplomskiapp.details.DetailsActivity
import kotlinx.android.synthetic.main.fragment_list.*

class ListActivity : AppCompatActivity(), ModelObserver, ActivityContract {

    companion object {

        const val POKEMON_SELECTED = 100
        const val REFRESH = 101
    }

    private var model: ListModel? = null
    private var view: ListFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        model = ListModel()
        view = ListFragment.newInstance(model!!)

        model?.subscribe(this)

        supportFragmentManager.beginTransaction().also {
            it.replace(android.R.id.content, view!!)
            it.commit()
        }

        setSupportActionBar(toolbar_top)
        window.statusBarColor = ContextCompat.getColor(
            this,
            R.color.black
        )
    }

    override fun onResume() {
        super.onResume()

        model?.fetchPokemons()
    }

    override fun update(flag: Int) {
        if (flag == ListModel.POKEMON) {
            val intent = Intent(this, DetailsActivity::class.java)
            intent.putExtra(DetailsActivity.POKEMON_EXTRA, model?.pokemon!!)

            startActivity(intent)
        }
    }

    override fun showLoading(load: Boolean) {

    }

    override fun handleEvent(event: Int, value: Any) {
        if (event == POKEMON_SELECTED) {
            val name = value as String
            model?.fetchPokemon(name)
        } else if (event == REFRESH) {
            model?.fetchPokemons()
        }
    }

    override fun finishActivity() {
        finish()
    }
}
