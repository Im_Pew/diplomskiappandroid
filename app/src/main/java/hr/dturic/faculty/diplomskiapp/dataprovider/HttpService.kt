package hr.dturic.faculty.diplomskiapp.dataprovider

import hr.dturic.faculty.diplomskiapp.models.Pokemon
import hr.dturic.faculty.diplomskiapp.models.PokemonListResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface HttpService {

    @GET("https://pokeapi.co/api/v2/pokemon?limit=151")
    fun fetchPokemons(): Call<PokemonListResponse>

    @GET("https://courses.cs.washington.edu/courses/cse154/webservices/pokedex/pokedex.php?")
    fun fetchPokemon(@Query("pokemon") name: String): Call<Pokemon>
}