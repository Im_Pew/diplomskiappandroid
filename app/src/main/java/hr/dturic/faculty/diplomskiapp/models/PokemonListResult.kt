package hr.dturic.faculty.diplomskiapp.models

import com.google.gson.annotations.SerializedName

data class PokemonListResult(
    @SerializedName("name") var name: String,
    @SerializedName("url") var url: String
) {

    companion object {
        private const val SPRITES_URL =
            "https://courses.cs.washington.edu/courses/cse154/webservices/pokedex/sprites/[%pok].png"
    }

    fun convert(): PokemonList {
        val splited = url.split("/")

        val id = splited[splited.size - 2].toInt()

        val url = SPRITES_URL.replace("[%pok]", name)

        val words = name.replace("-", " ")
            .split(" ")
            .toMutableList()

        var name = ""

        for (word in words) {
            name += word.capitalize() + " "
        }

        name = name.trim()

        return PokemonList(id, name, this.name, url)
    }
}
