package hr.dturic.faculty.diplomskiapp.models

data class PokemonList(
    var id: Int,
    var name: String,
    var codeName: String,
    var url: String
)