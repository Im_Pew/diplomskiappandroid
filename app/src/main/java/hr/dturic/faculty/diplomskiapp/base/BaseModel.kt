package hr.dturic.faculty.diplomskiapp.base

open class BaseModel {
    private var subscribers = ArrayList<ModelObserver>()

    fun subscribe(observer: ModelObserver) {
        subscribers.add(observer)
    }

    fun unsubscribe(observer: ModelObserver) {
        subscribers.remove(observer)
    }

    fun notify(flag: Int) {
        subscribers.forEach {
            it.update(flag)
        }
    }

    fun loading(loading: Boolean) {
        subscribers.forEach {
            it.showLoading(loading)
        }
    }
}