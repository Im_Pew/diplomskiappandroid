package hr.dturic.faculty.diplomskiapp.details

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import hr.dturic.faculty.diplomskiapp.R
import hr.dturic.faculty.diplomskiapp.base.ActivityContract
import hr.dturic.faculty.diplomskiapp.models.Pokemon

class DetailsActivity : AppCompatActivity(), ActivityContract {

    companion object {

        const val POKEMON_NAME_EXTRA = "pokemon_name"
        const val POKEMON_EXTRA = "pokemon"
    }

    private var model: DetailsModel? = null
    private var view: DetailsFragment? = null
    private var pokemonName: String? = null
    private var pokemon: Pokemon? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        when {
            intent.hasExtra(POKEMON_NAME_EXTRA) ->
                pokemonName = intent.getStringExtra(POKEMON_NAME_EXTRA)
            intent.hasExtra(POKEMON_EXTRA) ->
                pokemon = intent.getSerializableExtra(POKEMON_EXTRA) as Pokemon
            else -> finish()
        }

        model = DetailsModel()
        view = DetailsFragment.newInstance(model!!, pokemon)

        window.statusBarColor = ContextCompat.getColor(
            this@DetailsActivity,
            R.color.red
        )

        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            this.setBackgroundDrawable(
                ColorDrawable(
                    ContextCompat.getColor(
                        this@DetailsActivity,
                        R.color.red
                    )
                )
            )
            title = pokemon?.name ?: ""
        }

        supportFragmentManager.beginTransaction().also {
            it.replace(android.R.id.content, view!!)
            it.commit()
        }
    }

    override fun onResume() {
        super.onResume()

//        if (pokemonName != null)
//            model?.fetchPokemon(pokemonName!!)
    }

    override fun handleEvent(event: Int, value: Any) {

    }

    override fun finishActivity() {
        finish()
    }
}
