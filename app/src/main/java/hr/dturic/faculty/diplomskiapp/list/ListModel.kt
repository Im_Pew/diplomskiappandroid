package hr.dturic.faculty.diplomskiapp.list

import hr.dturic.faculty.diplomskiapp.base.BaseModel
import hr.dturic.faculty.diplomskiapp.dataprovider.DataProvider
import hr.dturic.faculty.diplomskiapp.models.Pokemon
import hr.dturic.faculty.diplomskiapp.models.PokemonList
import hr.dturic.faculty.diplomskiapp.models.PokemonListResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListModel : BaseModel() {

    companion object {

        const val POKEMONS = 100
        const val POKEMON = 101
    }

    var pokemons = ArrayList<PokemonList>()
    var pokemon: Pokemon? = null

    fun fetchPokemons() {
        loading(true)

        DataProvider.fetchPokemons().enqueue(object : Callback<PokemonListResponse> {
            override fun onFailure(call: Call<PokemonListResponse>, t: Throwable) {

                loading(false)
            }

            override fun onResponse(
                call: Call<PokemonListResponse>,
                response: Response<PokemonListResponse>
            ) {

                val pokemons = ArrayList<PokemonList>()

                response.body()?.results?.forEach { pokemon ->
                    pokemons.add(pokemon.convert())
                }

                this@ListModel.pokemons = pokemons

                notify(POKEMONS)
                loading(false)
            }
        })
    }

    fun fetchPokemon(name: String) {
        loading(true)

        DataProvider.fetchPokemon(name).enqueue(object : Callback<Pokemon> {
            override fun onFailure(call: Call<Pokemon>, t: Throwable) {

                loading(false)
            }

            override fun onResponse(call: Call<Pokemon>, response: Response<Pokemon>) {
                pokemon = response.body()
                notify(POKEMON)
                loading(false)
            }
        })
    }
}