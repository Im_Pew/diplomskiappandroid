package hr.dturic.faculty.diplomskiapp.details

import hr.dturic.faculty.diplomskiapp.base.BaseModel
import hr.dturic.faculty.diplomskiapp.dataprovider.DataProvider
import hr.dturic.faculty.diplomskiapp.models.Pokemon
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailsModel: BaseModel() {

    companion object {
        const val POKEMON = 101
    }

    var pokemon: Pokemon? = null

    fun fetchPokemon(name: String) {
        DataProvider.fetchPokemon(name).enqueue(object: Callback<Pokemon> {
            override fun onFailure(call: Call<Pokemon>, t: Throwable) {

            }

            override fun onResponse(call: Call<Pokemon>, response: Response<Pokemon>) {

                this@DetailsModel.pokemon = response.body()

                notify(POKEMON)
            }
        })
    }
}