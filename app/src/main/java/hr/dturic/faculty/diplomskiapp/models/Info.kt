package hr.dturic.faculty.diplomskiapp.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Info(
    @SerializedName("id") var id: String,
    @SerializedName("type") var type: String,
    @SerializedName("weakness") var weakness: String,
    @SerializedName("description") var description: String
): Serializable
