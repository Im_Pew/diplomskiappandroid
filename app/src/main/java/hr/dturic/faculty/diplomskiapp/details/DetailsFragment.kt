package hr.dturic.faculty.diplomskiapp.details

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import hr.dturic.faculty.diplomskiapp.R
import hr.dturic.faculty.diplomskiapp.base.ActivityContract
import hr.dturic.faculty.diplomskiapp.base.LoadingFragment
import hr.dturic.faculty.diplomskiapp.base.ModelObserver
import hr.dturic.faculty.diplomskiapp.models.Pokemon
import kotlinx.android.synthetic.main.fragment_details.*

class DetailsFragment : Fragment(), ModelObserver {

    companion object {

        fun newInstance(model: DetailsModel) = DetailsFragment().also {
            it.model = model
        }

        fun newInstance(model: DetailsModel, pokemon: Pokemon?) = DetailsFragment().also {
            it.model = model
            it.pokemon = pokemon
        }
    }

    private var model: DetailsModel? = null
    private var contract: ActivityContract? = null
    private var pokemon: Pokemon? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is ActivityContract) {
            contract = context
        }

        model?.subscribe(this)
    }

    override fun onDetach() {
        model?.unsubscribe(this)
        contract = null

        super.onDetach()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) contract?.finishActivity().also { return true }
        return super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        showLoading(true)
        prepareView()
    }

    private fun prepareView() {
        pokemonName.text = pokemon?.name
        pokemonHp.text = pokemon?.hp.toString()
        pokemonDescription.text = pokemon?.info?.description

        Glide.with(this).load(pokemon?.images?.getPhoto()).into(pokemonImage)
        Glide.with(this).load(pokemon?.images?.getTypeIcon()).into(typeIcon)
        Glide.with(this).load(pokemon?.images?.getWeaknessIcon()).into(weaknessIcon)
    }

    override fun showLoading(load: Boolean) {
        if (load) {
            childFragmentManager.beginTransaction()
                .add(content.id, LoadingFragment(), "loading")
                .commit()
        } else {
            val loader = childFragmentManager.findFragmentByTag("loading")
            if (loader != null) childFragmentManager.beginTransaction().remove(loader).commit()
        }
    }

    override fun update(flag: Int) {
        if (flag == DetailsModel.POKEMON) {
//            showLoading(false)
            pokemon = model?.pokemon
            prepareView()
        }
    }
}
