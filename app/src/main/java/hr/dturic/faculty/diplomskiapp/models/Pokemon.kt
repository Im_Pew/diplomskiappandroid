package hr.dturic.faculty.diplomskiapp.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Pokemon(
    @SerializedName("name") var name: String,
    @SerializedName("hp") var hp: Int,
    @SerializedName("info") var info: Info,
    @SerializedName("images") var images: Images
): Serializable
